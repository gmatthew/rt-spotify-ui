	var currentData = [];
	var selectedGenres = [];
	var multiselectorId = '#multiselector';
	var maxGenres = 50;

	var socket = io('http://spotify.gerard.co:8082');
	socket.on('connection', function(socket){
		console.log('connected');
	});

	socket.on('rmessage', function(data) { $('.debug').append(data+'<br>'); });

	socket.on('message', function (data) {
		
		if (currentData != data) {
			
			
			//Update current data
			currentData = data;

			//
			refresh();

			//build drop down
			buildMultiSelector(multiselectorId);

			//display filter section
			$('.filter').css('display','block');

			//Last Updated
			var time = new Date();
			$('.date-holder').text(time.toString());

		}

	});



	function updateView(sortedSongs) {

		$('.content').empty();

		for (var i = 0; i < sortedSongs.length; i++) {
			
			if (i >= 20) break;

			var song = sortedSongs[i];
			var artist_link = song.iframe;
			var genre = ((song.genre) && (song.genre != 'null') )? toTitleCase(song.genre) : 'Unknown';

			var number = i +1;
			number = (number < 10) ? '0'+number : number;

			
			
			var twitter_url_links = '';	
			for (var j = 0; j < song.tweets.length; j++) {
				twitter_url_links += '<a target=_blank href="'+song.tweets[j].url+'"><span class="glyphicon glyphicon-link" aria-hidden="true"></span></a>';
			}

					
			$('.content').append('<div class="song" data-genre="'+song.genre+'">'+
				'<img src="'+song.thumbnail+'"><br>'+
				''+artist_link+''+
				'<p><span class="number">#'+number+'</span> | <span class="title">'+song.song_title+'</span></p>'+
				'<p class="genre">'+genre+' &nbsp;&nbsp;'+twitter_url_links+'</p>'+
				'</div>');

		};


	}

	function buildMultiSelector(id) {
		var genres = getGenres();
		var sortedGenres = Object.keys(genres).sort(function(a,b){return genres[b]-genres[a]});
		var top10Genres = sortedGenres.splice(1,maxGenres).sort();

		var $select = $(id).empty();

		$.each(top10Genres, function (i,genre) {
			$select.append($("<option>").attr('value',genre).text(genre));
		});
		

		$select.multiselect('rebuild');
		$select.multiselect('select',selectedGenres);
	}

	function refresh() {

		if (selectedGenres.length > 0) {
			//Obtain Tweets which match
			var selectedTweets = [];
			$.each(currentData, function(i,tweet) {
				if (_.contains(selectedGenres, tweet.genre)) {
					selectedTweets.push(tweet);
				}
			});

			//Update View
			if (selectedTweets.length > 0) {
				updateView(selectedTweets);
				
				return;
			}
			
		}

		updateView(currentData);
	}

	function getGenres() {
		var genres = {};
		$.each(currentData, function (i,e) {
			var genre = e.genre;
			if (typeof(genres[genre]) == 'undefined') {
				genres[genre] = 1;
			}
			else {
				genres[genre] += 1;
			}
		});

		return genres;
	}

	function toTitleCase(str)
	{
	    return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
	}
	
	$(function() {
		$(multiselectorId).multiselect({
                        onDropdownHidden: function (e) {


                                //Obtain Seleted Options
                                selectedGenres = [];
                                $( "select option:selected" ).each(function() {
                                        var genre = $(this).text();
                                        selectedGenres.push(genre);
                                });


                                refresh();


                        }
                });

	});
