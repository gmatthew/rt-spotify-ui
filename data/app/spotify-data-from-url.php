<?php

include "vendor/autoload.php";

use Audeio\Spotify\Oauth2\Client\Provider\Spotify;
use League\OAuth2\Client\Grant\RefreshToken;

class SpotifyClient {
    

    var $api;

    function __construct () {

       $oauthProvider = new Spotify(array(
            'clientId' => '9005b654c04f494fbd2a7b993dd7dee1',
            'clientSecret' => '56f4302029f04ab4a614e3a07127995f',
            'redirectUri' => 'http://localhost:8000',
            'scope' => [],
	    'grant_type' => 'authorization_code'
        ));
        
/*          $accessToken = $oauthProvider->getAccessToken(new RefreshToken(), array(
              'refresh_token' => 'BQD8MgJXAkdI1tNlHvrJ2MrqzWMrtd25pgvvjO8afnyUiJgeyLZohzoZCSiGP4NCywxrqafbhdKEchMyyRT03I3ix3oXi0Nz7tu4O93KW0FkaBsvUbywQHeanbD_O6KL_MERnbRI1A4'
          ))->accessToken;
*/
       //$accessToken = 'BQD8MgJXAkdI1tNlHvrJ2MrqzWMrtd25pgvvjO8afnyUiJgeyLZohzoZCSiGP4NCywxrqafbhdKEchMyyRT03I3ix3oXi0Nz7tu4O93KW0FkaBsvUbywQHeanbD_O6KL_MERnbRI1A4';
        $this->api = new \Audeio\Spotify\API();
  //      $this->api->setAccessToken($accessToken);
       
    }

    public function extract ($url) {

        $response = $this->get_web_page($url);
        $json = json_decode($response['content']);
        $trackId = $this->get_track_id($json->html);
        $genre = $this->get_genre($trackId);

        $json->genre = $genre;
        return $json;
    }

    private function get_track_id($html) {

        preg_match('/:(\w)*"/',$html, $matches);
        $trackId = $matches[0];
        $trackId = str_replace(':', '', $trackId);
        $trackId = str_replace('"', '', $trackId);

        return $trackId;
    }

    private function get_genre($trackId) {
      	try {
		$track = $this->api->getTrack($trackId);
        	$artists = $track->getArtists();
        	foreach ($artists as $artist) {
            		$artist_details = $this->api->getArtist($artist->getId());
            		$genres = $artist_details->getGenres();
			
			if (count($genres) > 0 ) {
            			return array_shift($genres);
			}
         	}

		$albumGenres = $track->getAlbum()->getGenres();
		return array_shift($albumGenres);
	}
	catch (Exception $e) { }


	
        return;
    }

    private function get_web_page( $url ) {
        $res = array();
        $options = array( 
            CURLOPT_RETURNTRANSFER => true,     // return web page 
            CURLOPT_HEADER         => false,    // do not return headers 
            CURLOPT_FOLLOWLOCATION => true,     // follow redirects 
            CURLOPT_USERAGENT      => "spider", // who am i 
            CURLOPT_AUTOREFERER    => true,     // set referer on redirect 
            CURLOPT_CONNECTTIMEOUT => 120,      // timeout on connect 
            CURLOPT_TIMEOUT        => 120,      // timeout on response 
            CURLOPT_MAXREDIRS      => 10,       // stop after 10 redirects 
        ); 
        $ch      = curl_init( $url ); 
        curl_setopt_array( $ch, $options ); 
        $content = curl_exec( $ch ); 
        $err     = curl_errno( $ch ); 
        $errmsg  = curl_error( $ch ); 
        $header  = curl_getinfo( $ch ); 
        curl_close( $ch ); 

        $res['content'] = $content;     
        $res['url'] = $header['url'];
        return $res; 
    }  

}

header('Content-Type: application/json');
$url = "http://embed.spotify.com/oembed/?url=".$_GET['url'];
$spotify = new \SpotifyClient();
$json = $spotify->extract($url);
echo json_encode($json);

