<!DOCTYPE html>
<html lang="en">
<head>
	<title>Spotify | Real-Time Trending Playlist</title>
	<script src="https://cdn.socket.io/socket.io-1.3.5.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.js"></script>
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" />
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" />
	<link href="https://maxcdn.bootstrapcdn.com/bootswatch/3.3.5/yeti/bootstrap.min.css" rel="stylesheet" />
	<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.3/underscore-min.js"></script>
	<script type="text/javascript" src="js/bootstrap-multiselect.js"></script>
	<script type="text/javascript" src="js/spotify.min.js"></script>
	<link rel="stylesheet" type="text/css" href="css/spotify.css" />
	<link rel="stylesheet" type="text/css" href="css/bootstrap-multiselect.css" />
</head>
<body>

<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">
      </a>
    </div>
  </div>
</nav>
	<div class="container-fluid">
		<div class="page-header">
		  <h1>Spotify | Real-Time Trending Playlist <small>via Twitter</small></h1>
		  <h4>Team: <i>Gerard, Sasi, Thomas</i>
		  <h5>Last Updated: <span class='date-holder'></span></h5>
		  <div class="filter">
		  	<h6>Filter By Genre:</h6>
		  	<select multiple="multiple" id="multiselector">
		  	</select>
		  </div>
		</div>
		<div class="content">
		One Moment...
		</div>
	</div>
	<div class="debug"></div>
</body>
</html>
