var _ = require('underscore');
var http = require('follow-redirects').http;
var redis = require('redis');
var io = require('socket.io')(8082);
var client = redis.createClient();
var redis_updater = redis.createClient();
var local_server = "http://spotify.gerard.co/";

//General Setup
var allSongs = [];
var redisKey = 'allSongs';
redis_updater.get(redisKey,function(err, reply) {	
	allSongs = JSON.parse(reply);
});

//Timing Parameters
var minutes_to_expire = 60 * 60 * 1000; // 1 hour
var refresh_ui_time =  30 * 1000; //30 seconds
var clean_up_cycle_time = 5 * 60 * 1000;  // 5 minutes
var persist_cycle_time = 60 * 1000; // Every minute
var io_channel = 'message';


//================================================================//
// Interval Setup
//================================================================//

//Refresh UI
setInterval(function() {

	//Sort the songs
	var sortedSongs = _.sortBy(allSongs, 'count').reverse();	

	io.emit(io_channel,sortedSongs);

	console.log('refreshed ui');
}, refresh_ui_time);

//Setup Persistence Cycle
setInterval(function(){
	//Store to redis - so that if failure, we can recover
   	redis_updater.set(redisKey, JSON.stringify(allSongs));

	console.log('updated data in redis');
}, persist_cycle_time);

//Cleanup Data
setInterval(function() {
	//Remove expired        
    pruneSongs();

    //Remove expired Tweets
    pruneTweets();

	console.log('pruned data');
}, clean_up_cycle_time);

//================================================================//
// Callbacks
//================================================================//

//Socket IO Setup
io.on('connection', function(socket) {
	 //Sort the songs
        var sortedSongs = _.sortBy(allSongs, 'count').reverse();
	socket.emit(io_channel, sortedSongs);
});

//Redis Message Handler
client.on("message", function(channel, message) {
	
	var messageObj = JSON.parse(message);

	//Add song received from redis
	addSong(messageObj);

});

//Redis - Subscribe to Twitter Channel
client.subscribe("twitter");

//================================================================//
// Methods
//================================================================//
/*

	Purpose: 
		- Add song received from Redis

	Returns: 
		- Nothing

*/
function addSong(song) {
	var record = _.findWhere(allSongs, {song_title:song.song_title});
	
	if (record) {
		record.count++;
		record.tweets = record.tweets.concat(song.tweets);
		record.timestamp = song.timestamp;
	}
	else {
		allSongs.push(song);
	}
}


/*

	Purpose: 
		- Remove Expired Songs from Memory

	Returns: 
		- Nothing

*/
function pruneSongs() {

	var valid_from = (new Date).getTime() - minutes_to_expire;
	var sorted_by_timestamp = _.sortBy(allSongs, 'timestamp');
	
	var delete_up_to = -1;
	for (var i = 0; i < sorted_by_timestamp.length; i++) {
		if (sorted_by_timestamp[i].timestamp > valid_from) {
			delete_up_to = i;
			break;
		}
	}

	if (delete_up_to > 0) {
		sorted_by_timestamp.splice(0, delete_up_to);
		allSongs = sorted_by_timestamp;
	}

}

/*

	Purpose: 
		- Remove Expired Tweets

	Returns: 
		- Nothing

*/
function pruneTweets() {

	var valid_from = (new Date).getTime() - minutes_to_expire;

	//Update the tweets of remaining
	
	for (var i = 0; i < allSongs.length; i++){
		var valid_tweets = [];
		for (var j = 0; j < allSongs[i].tweets.length; j++) {
			tweet = allSongs[i].tweets[j];
			if (tweet.timestamp > valid_from) {
				valid_tweets.push(tweet);
			}
		}

		allSongs[i].tweets = valid_tweets;
		allSongs[i].count = valid_tweets.length;
	};

}
